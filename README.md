# Scripts

One-off scripts for doing stuff in Egora

Use the Makefile to run scripts.

## Server Notices

The script takes two or three arguments, the message and the access token, and an optional list of users. The access token cab be retrieved from the bottom of the Help & About section of your user settings.

`make run MSG=test TOKEN=access_token`

You can also target specific users, mostly for testing purposes.

`make run MSG=test TOKEN=access_token USERS=@charlemagne:egora.org`

`make run MSG=test TOKEN=access_token USERS=@charlemagne:egora.org,@mathias:egora.org,@jgreenriver:egora.org`

## Docker

The docker-compose file will run a postgres container restored to a backup of the production database. It will also run a synapse container configured as production is. You will have to first obtain a backup from production. Use `scp` to take a file from /var/lib/postgresql/backups/ and name it dump.sql on your local machine. You will also need to obtain homeserver.yaml and homeserver.signing.key from /etc/matrix-synapse/ 

Do not version a database dump or homeserver.yaml and the signing key. These contain secrets.

You will need to modify homeserver.yaml slightly. In the `database` key in the config, the `host` arg will need to be set to `host: postgres` in order to map to the docker container. The addresses bound on `port: 8008` will need to be set to `bind_addresses: ['127.0.0.1']` only.

Run `docker-compose up` to startup the containers. You can check postgres by running `psql -U postgres -h localhost` and logging in with the password in the docker-compose file (supersekritpassword).

## Egora Manifest

This is the manifest of the configuration of software and procedures currently in operation:

1) Synapse

Installed via the pre-built package method:

https://github.com/matrix-org/synapse/blob/develop/INSTALL.md#debianubuntu

Optional dependencies are installed to support link previews:

https://github.com/matrix-org/synapse/blob/93c8b077ed406ae8d68a68c05f669642a0dec4d2/INSTALL.md#url-previews

The installation process is:

```
apt-get update
apt-get install libxml2-dev
apt-get install python3-pip
pip3 install lxml
pip3 install netaddr
```

The homeserver.yaml was edited to include:

```
url_preview_enabled: true
url_preview_ip_range_blacklist:
- '127.0.0.0/8'
- '10.0.0.0/8'
- '172.16.0.0/12'
- '192.168.0.0/16'
- '100.64.0.0/10'
- '169.254.0.0/16'
- '::1/128'
- 'fe80::/64'
- 'fc00::/7'
```

```
room_list_publication_rules:
  - user_id: "@charlemagne:egora.org"
    alias: "*"
    room_id: "*"
    action: allow
  - user_id: "@jgreenriver:egora.org"
    alias: "*"
    room_id: "*"
    action: allow
  - user_id: "@mathias:egora.org"
    alias: "*"
    room_id: "*"
    action: allow
  - user_id: "@wolf:egora.org"
    alias: "*"
    room_id: "*"
    action: allow
```

For server notices, an `@server:egora.org` user was created:

`PUT https://matrix.egora.org:8448/_synapse/admin/v2/users/@server:egora.org`
{
    "password": "********",
    "displayname": "Egora Server Notices",
    "admin": false,
    "deactivated": false
}

For dimension, a `@dimension:egora.org` user was created:

`PUT https://matrix.egora.org:8448/_synapse/admin/v2/users/@dimension:egora.org`
{
    "password": "********",
    "displayname": "Egora Dimension Bot",
    "admin": false,
    "deactivated": false
}

2) Postgres

Installed via apt:

```
sudo apt update
sudo apt install postgresql postgresql-contrib
```

The pre-built package for Synapse includes the neessary Python library, but an additional library must be installed:

```
/opt/venvs/matrix-synapse/bin/pip install matrix-synapse[postgres]
```

A `synapse_user` is created in Postgres

The Postgres database is created by becoming the postgres user `su - postgres` and creating the database with psql:

```
CREATE DATABASE synapse
ENCODING 'UTF8'
LC_COLLATE='C'
LC_CTYPE='C'
template=template0
OWNER synapse_user;
```

The conf file  /etc/postgresql/12/main/pg_hba.conf was edited to include the line:

`host    synapse         synapse_user             ::1/128     md5`

This line must appear before the line:

`host    all         all             ::1/128     ident`

3) Cron tasks under the postgres user account on the synapse host machine:

```
0 0 * * * pg_dumpall > /var/lib/postgresql/backups/pg_backup_$(date "+\%Y-\%m-\%d-\%T")
0 1 * * * find . -mtime +6 -regex '.*/backups/pg_backup_[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]-[0-9][0-9]:[0-9][0-9]:[0-9][0-9]' -delete
```

