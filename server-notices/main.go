package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"
)

type Users struct {
	Users []User `json: "user"`
}

type User struct {
	Name        string `json:"name"`
	Deactivated int    `json:"deactivated"`
}

type ServerNotice struct {
	UserID  string  `json:"user_id"`
	Content Content `json:"content"`
}

type Content struct {
	MsgType string `json:"msgtype"`
	Body    string `json:"body"`
}

func main() {
	m := flag.String("m", "This is an egora server notice test message.", "a message string")
	token := flag.String("token", "token", "an access token")
	usernames := flag.String("users", "", "a comma-delimited list of usernames")
	flag.Parse()

	var usernameList []string
	if *usernames != "" {
		usernameList = strings.Split(*usernames, ",")
	}

	client := &http.Client{
		Timeout: 3 * time.Second,
	}

	req, err := http.NewRequest("GET", "https://matrix.egora.org:8448/_synapse/admin/v2/users?from=0&limit=1000&guests=false", nil)
	req.Header.Add("Authorization", "Bearer "+*token)
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}

	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		log.Println(resp.StatusCode)
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Println(err)
		}

		log.Fatal(string(body))
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	var users Users
	err = json.Unmarshal(body, &users)
	if err != nil {
		log.Fatal(err)
	}

	for _, u := range users.Users {
		if u.Deactivated == 0 && (len(usernameList) == 0 || contains(usernameList, u.Name)) {
			notice := ServerNotice{
				UserID: u.Name,
				Content: Content{
					MsgType: "m.text",
					Body:    *m,
				},
			}

			j, err := json.Marshal(notice)
			if err != nil {
				fmt.Printf("Error marshalling notice to %s: %s", u.Name, err.Error())
				continue
			}

			b := bytes.NewReader(j)
			req, err := http.NewRequest("POST", "https://matrix.egora.org:8448/_synapse/admin/v1/send_server_notice", b)
			req.Header.Add("Authorization", "Bearer "+*token)
			resp, err := client.Do(req)
			if err != nil {
				fmt.Printf("Error sending to %s: %s", u.Name, err.Error())
			}

			if resp.StatusCode != http.StatusOK {
				fmt.Printf("Non-200 response sending to %s: %d", u.Name, resp.StatusCode)
				body, err = ioutil.ReadAll(resp.Body)
				if err != nil {
					fmt.Printf("Error parsing non-200 response body: %s", err.Error())
				}

				fmt.Printf("Error sending to %s: %s", u.Name, string(body))
			}

			resp.Body.Close()
			fmt.Println(u.Name)
		}
	}
}
func contains(list []string, s string) bool {
	for _, v := range list {
		if v == s {
			return true
		}
	}
	return false
}
