
MSG ?= This is an egora server notice test message.
TOKEN ?= token
USERS ?= $()

.PHONY: run
run:
	go run ./server-notices/main.go -m=$(MSG) -token=$(TOKEN) -users=$(USERS)
